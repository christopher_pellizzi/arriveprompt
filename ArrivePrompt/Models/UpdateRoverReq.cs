﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArrivePrompt.Models
{
  public class UpdateRoverReq
  {
    [MaxLength(10)]
    public string RoverId { get; set; }
    [MaxLength(100)]
    public string MovementInstruction { get; set; }
  }
}