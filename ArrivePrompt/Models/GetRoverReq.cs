﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArrivePrompt.Models
{
  public class GetRoverReq
  {
    [MaxLength(10)]
    public string RoverId { get; set; }
  }
}