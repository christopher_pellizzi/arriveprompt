﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArrivePrompt.Models
{
  public class UpdateRoverResp
  {
    public string Message { get; set; }
    public string CurrentPosition { get; set; }
  }
}