﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ArrivePrompt.Models;

namespace ArrivePrompt.Controllers
{
  public class RoverController : ApiController
  {

    public static RoverManager rovers = new RoverManager();

    /// <summary>
    /// get the location of a rover by id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public GetRoverResp Get(string id)
    {
      return rovers.GetRover(id);
    }

    /// <summary>
    /// update the rover's position and return current location
    /// </summary>
    /// <param name="req"></param>
    /// <returns></returns>
    public UpdateRoverResp Post([FromBody]UpdateRoverReq req)
    {
      if (ModelState.IsValid)
      {
        return rovers.UpdateRover(req.RoverId, req.MovementInstruction);
      }

      return new UpdateRoverResp {Message = "invalid parameters entered"};
    }

  }
}