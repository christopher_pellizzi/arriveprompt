﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ArrivePrompt.Models;

namespace ArrivePrompt
{
  public class RoverManager
  {


    public static Dictionary<string, Position> rovers = new Dictionary<string, Position>();

    public class Position
    {
      public int x { get; set; } = 0;
      public int y { get; set; } = 0;

      //0=north 1=east 2=south 3=west
      public int facing = 0;

      public void ApplyInstruction(string inst)
      {
        switch (inst)
        {
          case "R":
            if (facing < 3)
              facing++;
            else
              facing = 0;
            break;
          case "L":
            if (facing > 0)
              facing--;
            else
              facing = 3;
            break;
          case "M":
            switch (facing)
            {
              case 0:
                y++;
                break;
              case 1:
                x++;
                break;
              case 2:
                y--;
                break;
              case 3:
                x--;
                break;
            }
            break;
        }
      }

      public void ApplyInstructionSet(string instructions)
      {
        foreach (var c in instructions)
        {
          ApplyInstruction(c.ToString());
        }
      }
    }

    public GetRoverResp GetRover(string id)
    {
      if (rovers.ContainsKey(id))
      {
        var rover = rovers[id];
        return new GetRoverResp {Message = "Rover found", CurrentPosition = $"({rover.x},{rover.y})"};
      }

      return new GetRoverResp {Message = "Rover not found"};
    }

    public UpdateRoverResp UpdateRover(string id, string instructionSet)
    {
      if (rovers.ContainsKey(id))
      {
        var rover = rovers[id];
        rover.ApplyInstructionSet(instructionSet);
        return new UpdateRoverResp { Message = "Rover position updated", CurrentPosition = $"({rover.x},{rover.y})" };
      }
      else
      {
        var rover = new Position();
        rover.ApplyInstructionSet(instructionSet);
        rovers.Add(id, rover);
        return new UpdateRoverResp { Message = "New rover created and moved", CurrentPosition = $"({rover.x},{rover.y})" };
      }
    }


  }
}